package converter

import (
	"reflect"
)

//Class permet de classifier les types de données.
//On y retrouve :
//- Les classes standard implémentées dans reflect: Ptr, Struct, String, etc.
//- Des sur-ensembles de classes: Element, Number, Collection, etc.
//- Des sous-emsembles de classes (nécessitant d’analyser le type plus en profondeur): [*]Slice, [*]Map et [*]Set
//- Des classes décrivant l’état de la valeur de la donnée : Zero et Nil
//
//À noter que certains types de données reflect ne sont pas géré par converter: Chan, Func, etc.
//Par ailleurs, la terminologie pour les type de nombres est quelque peut différente par rapport à reflect :
//- Int regroupe tous les type d’entiers (int, int8, int16, int32, int64)
//- Float regroupe tous les nombres à virgule flottante (float32, float64)
//- Contrairement à reflect, une distinction est faite entre les entiers purs et les entiers décrivant des caractères (rune et byte)
//
//Concernant les variables de type map[typeKey]typeValue, seules sont supportées :
//- map[string]elementType (regroupées sous la classe Map)
//- map[elementType]bool (regroupées sous la classe Set)
//Tous les autres types de map sont rattachés uniquement à la classe Collection.
type Class uint

const (
	Invalid Class = iota
	Zero
	Nil
	Ptr
	Struct
	Interface
	Element
	Number
	Char
	Collection
	Slice
	Map
	Set
	Bool
	Int
	Uint
	Float
	Rune
	Byte
	String
	GenericSlice
	BoolSlice
	IntSlice
	UintSlice
	FloatSlice
	RuneSlice
	ByteSlice
	StringSlice
	GenericMap
	BoolMap
	IntMap
	UintMap
	FloatMap
	RuneMap
	ByteMap
	StringMap
	GenericSet
	BoolSet
	IntSet
	UintSet
	FloatSet
	RuneSet
	ByteSet
	StringSet
)

//Classes est un ensemble de classes
type Classes map[Class]bool

func NewClasses(classes ...Class) Classes {
	out := make(Classes)
	return out.Add(classes...)
}

func (l Classes) Add(classes ...Class) Classes {
	for _, c := range classes {
		l[c] = true
	}
	return l
}

func (l Classes) Remove(classes ...Class) Classes {
	for _, c := range classes {
		delete(l, c)
	}
	return l
}

//Checker est une structure permettant d’analyser finement les propriétés d’une variable
//en recherchant les classes auxquelles elle appartient.
type Checker struct {
	e interface{}
	v reflect.Value
	t reflect.Type
	k reflect.Kind
}

func (c *Checker) Value() reflect.Value { return c.v }
func (c *Checker) Kind() reflect.Kind   { return c.k }
func (c *Checker) Type() reflect.Type   { return c.t }

//KindClass retourne la liste des classes de premier niveau
func (c *Checker) KindClass() Classes {
	out := NewClasses()
	switch c.Kind() {
	case reflect.Invalid:
		out.Add(Invalid)
	case reflect.Ptr:
		out.Add(Ptr, Element)
	case reflect.Interface:
		out.Add(Interface, Element)
	case reflect.Struct:
		out.Add(Struct, Element)
	case reflect.Bool:
		out.Add(Bool, Element)
	case reflect.String:
		out.Add(String, Element)
	case reflect.Int8:
		out.Add(Byte, Int, Number, Element, Char)
	case reflect.Int32:
		out.Add(Rune, Int, Number, Element, Char)
	case reflect.Int, reflect.Int16, reflect.Int64:
		out.Add(Int, Number, Element)
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		out.Add(Uint, Number, Element)
	case reflect.Float32, reflect.Float64:
		out.Add(Float, Number, Element)
	case reflect.Slice:
		out.Add(Slice, Collection)
	case reflect.Map:
		out.Add(Collection)
		if c.Type().Key().Kind() == reflect.String {
			out.Add(Map)
		} else if c.Type().Elem().Kind() == reflect.Bool {
			out.Add(Set)
		}
	default:
		out.Add(Element)
	}
	return out
}

func (c *Checker) slice() Classes {
	out := NewClasses()
	switch c.Type().Elem().Kind() {
	case reflect.Interface:
		out.Add(GenericSlice)
	case reflect.Bool:
		out.Add(BoolSlice)
	case reflect.String:
		out.Add(StringSlice)
	case reflect.Int8:
		out.Add(ByteSlice, IntSlice, String)
	case reflect.Int32:
		out.Add(RuneSlice, IntSlice, String)
	case reflect.Int, reflect.Int16, reflect.Int64:
		out.Add(IntSlice)
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		out.Add(UintSlice)
	case reflect.Float32, reflect.Float64:
		out.Add(FloatSlice)
	}
	return out
}

//SliceClass retourne la sous-classe d’un élément de type Slice
func (c *Checker) SliceClass() Classes {
	if c.Kind() != reflect.Slice {
		return NewClasses()
	}
	return c.slice()
}

func (c *Checker) set() Classes {
	out := NewClasses()
	switch c.Type().Key().Kind() {
	case reflect.Interface:
		out.Add(GenericSet)
	case reflect.Bool:
		out.Add(BoolSet)
	case reflect.String:
		out.Add(StringSet)
	case reflect.Int8:
		out.Add(ByteSet, IntSet)
	case reflect.Int32:
		out.Add(RuneSet, IntSet)
	case reflect.Int, reflect.Int16, reflect.Int64:
		out.Add(IntSet)
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		out.Add(UintSet)
	case reflect.Float32, reflect.Float64:
		out.Add(FloatSet)
	}
	return out
}

//SetClass retourne la sous-classe d’un élément de type Set
func (c *Checker) SetClass() Classes {
	if c.Kind() != reflect.Map || c.Type().Elem().Kind() != reflect.Bool {
		return NewClasses()
	}
	return c.set()
}

func (c *Checker) maps() Classes {
	out := NewClasses()
	switch c.Type().Elem().Kind() {
	case reflect.Interface:
		out.Add(GenericMap)
	case reflect.Bool:
		out.Add(BoolMap)
	case reflect.String:
		out.Add(StringMap)
	case reflect.Int8:
		out.Add(ByteMap, IntMap)
	case reflect.Int32:
		out.Add(RuneMap, IntMap)
	case reflect.Int, reflect.Int16, reflect.Int64:
		out.Add(IntMap)
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		out.Add(UintMap)
	case reflect.Float32, reflect.Float64:
		out.Add(FloatMap)
	}
	return out
}

//MapClass retourne la sous-classe d’un élément de type Map
func (c *Checker) MapClass() Classes {
	if c.Kind() != reflect.Map || c.Type().Key().Kind() != reflect.String {
		return NewClasses()
	}
	return c.maps()
}

//IsZero est équivalent à la méthode IsZero de reflect.Value
//à la différence qu’aucun panic n’est généré en cas de valeur de type reflect.Invalid
func (c *Checker) IsZero() bool {
	v := c.Value()
	return v.Kind() != reflect.Invalid && v.IsZero()
}

//IsNil est équivalent à la méthode IsZero de reflect.Value
//à la différence qu’aucun panic n’est généré en cas de valeur non nilable.
func (c *Checker) IsNil() bool {
	switch c.Kind() {
	case reflect.Invalid:
		return true
	case reflect.Chan, reflect.Func, reflect.Map, reflect.Ptr, reflect.Slice, reflect.Interface:
		return c.Value().IsNil()
	}
	return false
}

//AllClasses retournes toutes les classes d’appartenance de la valeur :
//- Type général/sur-ensemble,
//- Sous-ensemble éventuel dans le cas des collections,
//- Classes d’état (Nil et Zero).
func (c *Checker) AllClasses() Classes {
	out := c.KindClass()
	var collection Classes
	if out[Slice] {
		collection = c.slice()
	} else if out[Set] {
		collection = c.set()
	} else if out[Map] {
		collection = c.maps()
	}
	if collection != nil {
		for e := range collection {
			out.Add(e)
		}
	}
	if c.IsZero() {
		out.Add(Zero)
	}
	if c.IsNil() {
		out.Add(Nil)
	}
	return out
}

func (c *Checker) IsInvalid() bool      { return c.Kind() == reflect.Invalid }
func (c *Checker) IsPointer() bool      { return c.Kind() == reflect.Ptr }
func (c *Checker) IsStruct() bool       { return c.Kind() == reflect.Struct }
func (c *Checker) IsInterface() bool    { return c.Kind() == reflect.Interface }
func (c *Checker) IsElement() bool      { return c.KindClass()[Element] }
func (c *Checker) IsNumber() bool       { return c.KindClass()[Number] }
func (c *Checker) IsChar() bool         { return c.KindClass()[Char] }
func (c *Checker) IsCollection() bool   { return c.KindClass()[Collection] }
func (c *Checker) IsSlice() bool        { return c.KindClass()[Slice] }
func (c *Checker) IsMap() bool          { return c.KindClass()[Map] }
func (c *Checker) IsSet() bool          { return c.KindClass()[Set] }
func (c *Checker) IsBool() bool         { return c.KindClass()[Bool] }
func (c *Checker) IsInt() bool          { return c.KindClass()[Int] }
func (c *Checker) IsUint() bool         { return c.KindClass()[Uint] }
func (c *Checker) IsFloat() bool        { return c.KindClass()[Float] }
func (c *Checker) IsRune() bool         { return c.KindClass()[Rune] }
func (c *Checker) IsByte() bool         { return c.KindClass()[Byte] }
func (c *Checker) IsString() bool       { return c.KindClass()[String] }
func (c *Checker) IsGenericSlice() bool { return c.AllClasses()[GenericSlice] }
func (c *Checker) IsBoolSlice() bool    { return c.AllClasses()[BoolSlice] }
func (c *Checker) IsIntSlice() bool     { return c.AllClasses()[IntSlice] }
func (c *Checker) IsUintSlice() bool    { return c.AllClasses()[UintSlice] }
func (c *Checker) IsFloatSlice() bool   { return c.AllClasses()[FloatSlice] }
func (c *Checker) IsRuneSlice() bool    { return c.AllClasses()[RuneSlice] }
func (c *Checker) IsByteSlice() bool    { return c.AllClasses()[ByteSlice] }
func (c *Checker) IsStringSlice() bool  { return c.AllClasses()[StringSlice] }
func (c *Checker) IsGenericMap() bool   { return c.AllClasses()[GenericMap] }
func (c *Checker) IsBoolMap() bool      { return c.AllClasses()[BoolMap] }
func (c *Checker) IsIntMap() bool       { return c.AllClasses()[IntMap] }
func (c *Checker) IsUintMap() bool      { return c.AllClasses()[UintMap] }
func (c *Checker) IsFloatMap() bool     { return c.AllClasses()[FloatMap] }
func (c *Checker) IsRuneMap() bool      { return c.AllClasses()[RuneMap] }
func (c *Checker) IsByteMap() bool      { return c.AllClasses()[ByteMap] }
func (c *Checker) IsStringMap() bool    { return c.AllClasses()[StringMap] }
func (c *Checker) IsGenericSet() bool   { return c.AllClasses()[GenericSet] }
func (c *Checker) IsBoolSet() bool      { return c.AllClasses()[BoolSet] }
func (c *Checker) IsIntSet() bool       { return c.AllClasses()[IntSet] }
func (c *Checker) IsUintSet() bool      { return c.AllClasses()[UintSet] }
func (c *Checker) IsFloatSet() bool     { return c.AllClasses()[FloatSet] }
func (c *Checker) IsRuneSet() bool      { return c.AllClasses()[RuneSet] }
func (c *Checker) IsByteSet() bool      { return c.AllClasses()[ByteSet] }
func (c *Checker) IsStringSet() bool    { return c.AllClasses()[StringSet] }

//NewChecker retourne un checker pour analyser le paramètre fourni.
func NewChecker(e interface{}) *Checker {
	v := reflect.ValueOf(e)
	return &Checker{
		e: e,
		v: v,
		k: v.Kind(),
		t: v.Type(),
	}
}

func IsZero(e interface{}) bool         { return (NewChecker(e)).IsZero() }
func IsNil(e interface{}) bool          { return (NewChecker(e)).IsNil() }
func IsInvalid(e interface{}) bool      { return (NewChecker(e)).IsInvalid() }
func IsPointer(e interface{}) bool      { return (NewChecker(e)).IsPointer() }
func IsStruct(e interface{}) bool       { return (NewChecker(e)).IsStruct() }
func IsInterface(e interface{}) bool    { return (NewChecker(e)).IsInterface() }
func IsElement(e interface{}) bool      { return (NewChecker(e)).IsElement() }
func IsNumber(e interface{}) bool       { return (NewChecker(e)).IsNumber() }
func IsChar(e interface{}) bool         { return (NewChecker(e)).IsChar() }
func IsCollection(e interface{}) bool   { return (NewChecker(e)).IsCollection() }
func IsSlice(e interface{}) bool        { return (NewChecker(e)).IsSlice() }
func IsMap(e interface{}) bool          { return (NewChecker(e)).IsMap() }
func IsSet(e interface{}) bool          { return (NewChecker(e)).IsSet() }
func IsBool(e interface{}) bool         { return (NewChecker(e)).IsBool() }
func IsInt(e interface{}) bool          { return (NewChecker(e)).IsInt() }
func IsUint(e interface{}) bool         { return (NewChecker(e)).IsUint() }
func IsFloat(e interface{}) bool        { return (NewChecker(e)).IsFloat() }
func IsRune(e interface{}) bool         { return (NewChecker(e)).IsRune() }
func IsByte(e interface{}) bool         { return (NewChecker(e)).IsByte() }
func IsString(e interface{}) bool       { return (NewChecker(e)).IsString() }
func IsGenericSlice(e interface{}) bool { return (NewChecker(e)).IsGenericSlice() }
func IsBoolSlice(e interface{}) bool    { return (NewChecker(e)).IsBoolSlice() }
func IsIntSlice(e interface{}) bool     { return (NewChecker(e)).IsIntSlice() }
func IsUintSlice(e interface{}) bool    { return (NewChecker(e)).IsUintSlice() }
func IsFloatSlice(e interface{}) bool   { return (NewChecker(e)).IsFloatSlice() }
func IsRuneSlice(e interface{}) bool    { return (NewChecker(e)).IsRuneSlice() }
func IsByteSlice(e interface{}) bool    { return (NewChecker(e)).IsByteSlice() }
func IsStringSlice(e interface{}) bool  { return (NewChecker(e)).IsStringSlice() }
func IsGenericMap(e interface{}) bool   { return (NewChecker(e)).IsGenericMap() }
func IsBoolMap(e interface{}) bool      { return (NewChecker(e)).IsBoolMap() }
func IsIntMap(e interface{}) bool       { return (NewChecker(e)).IsIntMap() }
func IsUintMap(e interface{}) bool      { return (NewChecker(e)).IsUintMap() }
func IsFloatMap(e interface{}) bool     { return (NewChecker(e)).IsFloatMap() }
func IsRuneMap(e interface{}) bool      { return (NewChecker(e)).IsRuneMap() }
func IsByteMap(e interface{}) bool      { return (NewChecker(e)).IsByteMap() }
func IsStringMap(e interface{}) bool    { return (NewChecker(e)).IsStringMap() }
func IsGenericSet(e interface{}) bool   { return (NewChecker(e)).IsGenericSet() }
func IsBoolSet(e interface{}) bool      { return (NewChecker(e)).IsBoolSet() }
func IsIntSet(e interface{}) bool       { return (NewChecker(e)).IsIntSet() }
func IsUintSet(e interface{}) bool      { return (NewChecker(e)).IsUintSet() }
func IsFloatSet(e interface{}) bool     { return (NewChecker(e)).IsFloatSet() }
func IsRuneSet(e interface{}) bool      { return (NewChecker(e)).IsRuneSet() }
func IsByteSet(e interface{}) bool      { return (NewChecker(e)).IsByteSet() }
func IsStringSet(e interface{}) bool    { return (NewChecker(e)).IsStringSet() }
