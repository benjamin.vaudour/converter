package converter

import (
	"fmt"
	"reflect"
	"sort"
	"strconv"
	"unicode/utf8"
)

//ValueOf retourne la valeur pointée paramètre d’entrée.
//Si le paramètre n’est pas un pointeur, elle
//retourne le paramètre lui-même.
func ValueOf(e interface{}) interface{} {
	c := NewChecker(e)
	if (c.KindClass())[Ptr] {
		return c.Value().Elem().Interface()
	}
	return e
}

func pointer(t reflect.Type) reflect.Value {
	return reflect.New(t)
}

//PointerOf retourne un pointeur vers
//une variable ayant la même valeur que le paramètre d’entrée.
func PointerOf(e interface{}) interface{} {
	c := NewChecker(e)
	p := pointer(c.Type())
	p.Elem().Set(c.Value())
	return p.Interface()
}

//ZeroOf retourne le zéro du type du paramètre d’entrée.
func ZeroOf(e interface{}) interface{} {
	c := NewChecker(e)
	if c.IsZero() {
		return e
	}
	return reflect.Zero(c.Type()).Interface()
}

func toSlice(src interface{}, dest *Checker) (ok bool) {
	t := dest.Type()
	p := pointer(t.Elem())
	if ok = Convert(src, p.Interface()); ok {
		e := reflect.MakeSlice(t, 1, 1)
		e.Index(0).Set(p.Elem())
		dest.Value().Set(e)
	}
	return
}

func toSet(src interface{}, dest *Checker) (ok bool) {
	t := dest.Type()
	p := pointer(t.Key())
	if ok = Convert(src, p.Interface()); ok {
		e := reflect.MakeMap(t)
		e.SetMapIndex(p.Elem(), reflect.ValueOf(true))
		dest.Value().Set(e)
	}
	return
}

func convertBool(src reflect.Value, dest *Checker) (ok bool) {
	s, c := src.Bool(), dest.KindClass()
	var vs reflect.Value
	switch {
	case c[Bool]:
		vs, ok = src, true
	case c[Number]:
		e := 0
		if s {
			e = 1
		}
		vs, ok = reflect.ValueOf(e), true
	case c[String]:
		vs, ok = reflect.ValueOf(strconv.FormatBool(s)), true
	case c[Slice]:
		return toSlice(s, dest)
	case c[Set]:
		return toSet(s, dest)
	}
	if ok {
		vs = vs.Convert(dest.Type())
		dest.Value().Set(vs)
	}
	return
}

func convertChar(src reflect.Value, dest *Checker) (ok bool) {
	s, c := rune(src.Int()), dest.KindClass()
	var vs reflect.Value
	switch {
	case c[Bool]:
		vs, ok = reflect.ValueOf(s != 0), true
	case c[Number]:
		vs, ok = src, true
	case c[String]:
		vs, ok = reflect.ValueOf(fmt.Sprintf("%c", s)), true
	case c[Slice]:
		return toSlice(s, dest)
	case c[Set]:
		return toSlice(s, dest)
	}
	if ok {
		vs = vs.Convert(dest.Type())
		dest.Value().Set(vs)
	}
	return
}

func convertInt(src reflect.Value, dest *Checker) (ok bool) {
	s, c := src.Int(), dest.KindClass()
	var vs reflect.Value
	switch {
	case c[Bool]:
		vs, ok = reflect.ValueOf(s != 0), true
	case c[Number]:
		vs, ok = src, true
	case c[String]:
		vs, ok = reflect.ValueOf(strconv.FormatInt(s, 10)), true
	case c[Slice]:
		return toSlice(s, dest)
	case c[Set]:
		return toSlice(s, dest)
	}
	if ok {
		vs = vs.Convert(dest.Type())
		dest.Value().Set(vs)
	}
	return
}

func convertUint(src reflect.Value, dest *Checker) (ok bool) {
	s, c := src.Uint(), dest.KindClass()
	var vs reflect.Value
	switch {
	case c[Bool]:
		vs, ok = reflect.ValueOf(s != 0), true
	case c[Number]:
		vs, ok = src, true
	case c[String]:
		vs, ok = reflect.ValueOf(strconv.FormatUint(s, 10)), true
	case c[Slice]:
		return toSlice(s, dest)
	case c[Set]:
		return toSlice(s, dest)
	}
	if ok {
		vs = vs.Convert(dest.Type())
		dest.Value().Set(vs)
	}
	return
}

func convertFloat(src reflect.Value, dest *Checker) (ok bool) {
	s, c := src.Float(), dest.KindClass()
	var vs reflect.Value
	switch {
	case c[Bool]:
		vs, ok = reflect.ValueOf(s != 0), true
	case c[Number]:
		vs, ok = src, true
	case c[String]:
		vs, ok = reflect.ValueOf(strconv.FormatFloat(s, 'f', -1, 64)), true
	case c[Slice]:
		return toSlice(s, dest)
	case c[Set]:
		return toSlice(s, dest)
	}
	if ok {
		vs = vs.Convert(dest.Type())
		dest.Value().Set(vs)
	}
	return
}

func convertString(src reflect.Value, dest *Checker) (ok bool) {
	s, c := src.String(), dest.KindClass()
	var vs reflect.Value
	switch {
	case c[Bool]:
		e, err := strconv.ParseBool(s)
		if ok = err == nil; ok {
			vs = reflect.ValueOf(e)
		}
	case c[Byte]:
		if ok = len(s) == 1; ok {
			vs = reflect.ValueOf(s[0])
		}
	case c[Rune]:
		if ok = utf8.RuneCountInString(s) == 1; ok {
			e, _ := utf8.DecodeRuneInString(s)
			vs = reflect.ValueOf(e)
		}
	case c[Int]:
		e, err := strconv.ParseInt(s, 0, 64)
		if ok = err == nil; ok {
			vs = reflect.ValueOf(e)
		}
	case c[Uint]:
		e, err := strconv.ParseUint(s, 0, 64)
		if ok = err == nil; ok {
			vs = reflect.ValueOf(e)
		}
	case c[Float]:
		e, err := strconv.ParseFloat(s, 64)
		if ok = err == nil; ok {
			vs = reflect.ValueOf(e)
		}
	case c[String]:
		vs, ok = src, true
	case c[Slice]:
		return toSlice(s, dest)
	case c[Set]:
		return toSlice(s, dest)
	}
	if ok {
		vs = vs.Convert(dest.Type())
		dest.Value().Set(vs)
	}
	return
}

func convertSlice(src reflect.Value, dest *Checker) (ok bool) {
	l, c := src.Len(), dest.KindClass()
	var n reflect.Value
	switch {
	case c[Slice]:
		n = reflect.MakeSlice(dest.Type(), l, src.Cap())
		ok = true
		for i := 0; i < l; i++ {
			s, d := src.Index(i), n.Index(i)
			if ok = Convert(s.Interface(), d.Addr().Interface(), true); !ok {
				return
			}
		}
	case c[Set]:
		n, ok = reflect.MakeMap(dest.Type()), true
		t := dest.Type().Key()
		for i := 0; i < l; i++ {
			s, d := src.Index(i), pointer(t)
			if ok = Convert(s.Interface(), d.Interface(), true); !ok {
				return
			}
			n.SetMapIndex(d.Elem(), reflect.ValueOf(true))
		}
	case c[Map]:
		n, ok = reflect.MakeMap(dest.Type()), true
		t := dest.Type().Elem()
		for i := 0; i < l; i++ {
			s, d := src.Index(i), pointer(t)
			if ok = Convert(s.Interface(), d.Interface(), true); !ok {
				return
			}
			k := reflect.ValueOf(strconv.Itoa(i))
			n.SetMapIndex(k, d.Elem())
		}
	default:
		if ok = l == 1; ok {
			return Convert(src.Index(0).Interface(), dest.Value().Addr().Interface(), true)
		}
		return
	}
	dest.Value().Set(n)
	return
}

func convertMap(src reflect.Value, dest *Checker) (ok bool) {
	keys, c := src.MapKeys(), dest.KindClass()
	var n reflect.Value
	switch {
	case c[Slice]:
		idx := make([]int, len(keys))
		midx := make(map[string]int)
		ok = true
		for i, k := range keys {
			s := k.String()
			j, err := strconv.Atoi(s)
			if ok = err == nil; !ok {
				return
			}
			idx[i], midx[s] = j, j
		}
		sort.Ints(idx)
		for i, j := range idx {
			if ok = i == j; !ok {
				return
			}
		}
		n = reflect.MakeSlice(dest.Type(), len(keys), len(keys))
		t := dest.Type().Elem()
		for _, k := range keys {
			s, d := src.MapIndex(k), pointer(t)
			if ok = Convert(s.Interface(), d.Interface(), true); !ok {
				return
			}
			n.Index(midx[k.String()]).Set(d.Elem())
		}
	case c[Map]:
		n, ok = reflect.MakeMap(dest.Type()), true
		t := dest.Type().Elem()
		for _, k := range keys {
			s, d := src.MapIndex(k), pointer(t)
			if ok = Convert(s.Interface(), d.Interface(), true); !ok {
				return
			}
			n.SetMapIndex(k, d.Elem())
		}
	case c[Struct]:
		t := dest.Type()
		n, ok = pointer(t).Elem(), true
		for _, k := range keys {
			fs := k.String()
			if f, exists := t.FieldByName(fs); !exists || f.PkgPath != "" {
				continue
			}
			s, d := src.MapIndex(k), n.FieldByName(fs)
			if ok = d.CanAddr() && Convert(s.Interface(), d.Addr().Interface(), true); !ok {
				return
			}
		}
	default:
		return
	}
	dest.Value().Set(n)
	return
}

func convertSet(src reflect.Value, dest *Checker) (ok bool) {
	keys, c := src.MapKeys(), dest.KindClass()
	l := len(keys)
	var n reflect.Value
	switch {
	case c[Slice]:
		n, ok = reflect.MakeSlice(dest.Type(), l, l), true
		for i, s := range keys {
			d := n.Index(i)
			if ok = Convert(s.Interface(), d.Addr().Interface(), true); !ok {
				return
			}
		}
	case c[Set]:
		n, ok = reflect.MakeMap(dest.Type()), true
		t := dest.Type().Key()
		for _, s := range keys {
			d := pointer(t)
			if ok = Convert(s.Interface(), d.Interface(), true); !ok {
				return
			}
			n.SetMapIndex(d.Elem(), src.MapIndex(s))
		}
	case c[Struct]:
		t := dest.Type()
		n, ok = pointer(t).Elem(), true
		for _, k := range keys {
			fs := k.String()
			if f, exists := t.FieldByName(fs); !exists || f.PkgPath != "" {
				continue
			}
			s, d := src.MapIndex(k), n.FieldByName(fs)
			if ok = d.CanAddr() && Convert(s.Interface(), d.Addr().Interface(), true); !ok {
				return
			}
		}
	default:
		if ok = l == 1; ok {
			return Convert(src.MapIndex(keys[0]).Interface(), dest.Value().Addr().Interface(), true)
		}
		return
	}
	dest.Value().Set(n)
	return
}

func convertStruct(src reflect.Value, dest *Checker) (ok bool) {
	l, t, c := src.NumField(), src.Type(), dest.KindClass()
	var fields []reflect.StructField
	for i := 0; i < l; i++ {
		f := t.Field(i)
		if f.PkgPath == "" {
			fields = append(fields, f)
		}
	}
	var n reflect.Value
	switch {
	case c[Struct]:
		t := dest.Type()
		for _, f := range fields {
			if _, ok = t.FieldByName(f.Name); !ok {
				return
			}
		}
		n, ok = pointer(t).Elem(), true
		for _, f := range fields {
			s, d := src.FieldByName(f.Name), n.FieldByName(f.Name)
			if ok = d.CanAddr() && Convert(s.Interface(), d.Addr().Interface(), true); !ok {
				return
			}
		}
	case c[Map]:
		n, ok = reflect.MakeMap(dest.Type()), true
		t := dest.Type().Elem()
		for _, f := range fields {
			s, d := src.FieldByName(f.Name), pointer(t)
			if ok = Convert(s.Interface(), d.Interface(), true); !ok {
				return
			}
			n.SetMapIndex(reflect.ValueOf(f.Name), d.Elem())
		}
	case c[Set]:
		t := dest.Type()
		n, ok = reflect.MakeMap(t), true
		tk, tv := t.Key(), t.Elem()
		for _, f := range fields {
			s, k, v := src.FieldByName(f.Name), pointer(tk), pointer(tv)
			if ok = Convert(f.Name, k.Interface(), true) && Convert(s.Interface(), v.Interface(), true); !ok {
				return
			}
			n.SetMapIndex(k.Elem(), v.Elem())
		}
	default:
		return
	}
	dest.Value().Set(n)
	return
}

//Convert convertit la valeur du paramètre source vers
//le paramètre destination et retourne true si l’opération
//s’est effectuée avec succès.
//La destination doit être un pointeur, afin de pouvoir modifier
//sa valeur.
//Si le paramètre optionnel safe est fourni et vaut true,
//la destination n’est modifiée que si son type est équivalent
//à celui de la source (par exemple int vers int64, mais
//pas int vers string).
func Convert(src, dest interface{}, safe ...bool) bool {
	cs, cd := NewChecker(src), NewChecker(dest)
	if !cd.IsPointer() || cd.IsNil() {
		return false
	}
	ts, td := cs.Type(), cd.Type().Elem()
	vs, vd := cs.Value(), cd.Value().Elem()
	if ts.ConvertibleTo(td) && td.ConvertibleTo(ts) {
		vd.Set(vs.Convert(td))
		return true
	}
	if td.Kind() == reflect.Interface {
		vd.Set(vs)
		return true
	}
	if len(safe) != 0 && safe[0] {
		return false
	}
	cd.v, cd.t, cd.k = vd, td, vd.Kind()
	ccs := cs.KindClass()
	switch {
	case ccs[Bool]:
		return convertBool(vs, cd)
	case ccs[Char]:
		return convertChar(vs, cd)
	case ccs[Int]:
		return convertInt(vs, cd)
	case ccs[Uint]:
		return convertUint(vs, cd)
	case ccs[Float]:
		return convertFloat(vs, cd)
	case ccs[String]:
		return convertString(vs.Convert(reflect.TypeOf("")), cd)
	case ccs[Slice]:
		return convertSlice(vs, cd)
	case ccs[Map]:
		return convertMap(vs, cd)
	case ccs[Set]:
		return convertSet(vs, cd)
	case ccs[Struct]:
		return convertStruct(vs, cd)
	}
	return false
}

func ToInterface(src interface{}, safe ...bool) (dest interface{}, ok bool) {
	ok = Convert(src, &dest, safe...)
	return
}

func ToBool(src interface{}, safe ...bool) (dest bool, ok bool) {
	ok = Convert(src, &dest, safe...)
	return
}

func ToRune(src interface{}, safe ...bool) (dest rune, ok bool) {
	ok = Convert(src, &dest, safe...)
	return
}

func ToByte(src interface{}, safe ...bool) (dest byte, ok bool) {
	ok = Convert(src, &dest, safe...)
	return
}

func ToInt(src interface{}, safe ...bool) (dest int, ok bool) {
	ok = Convert(src, &dest, safe...)
	return
}

func ToUint(src interface{}, safe ...bool) (dest uint, ok bool) {
	ok = Convert(src, &dest, safe...)
	return
}

func ToFloat(src interface{}, safe ...bool) (dest float64, ok bool) {
	ok = Convert(src, &dest, safe...)
	return
}

func ToString(src interface{}, safe ...bool) (dest string, ok bool) {
	ok = Convert(src, &dest, safe...)
	return
}

func ToSlice(src interface{}, safe ...bool) (dest []interface{}, ok bool) {
	ok = Convert(src, &dest, safe...)
	return
}

func ToBoolSlice(src interface{}, safe ...bool) (dest []bool, ok bool) {
	ok = Convert(src, &dest, safe...)
	return
}

func ToRuneSlice(src interface{}, safe ...bool) (dest []rune, ok bool) {
	ok = Convert(src, &dest, safe...)
	return
}

func ToByteSlice(src interface{}, safe ...bool) (dest []byte, ok bool) {
	ok = Convert(src, &dest, safe...)
	return
}

func ToIntSlice(src interface{}, safe ...bool) (dest []int, ok bool) {
	ok = Convert(src, &dest, safe...)
	return
}

func ToUintSlice(src interface{}, safe ...bool) (dest []uint, ok bool) {
	ok = Convert(src, &dest, safe...)
	return
}

func ToFloatSlice(src interface{}, safe ...bool) (dest []float64, ok bool) {
	ok = Convert(src, &dest, safe...)
	return
}

func ToStringSlice(src interface{}, safe ...bool) (dest []string, ok bool) {
	ok = Convert(src, &dest, safe...)
	return
}

func ToMap(src interface{}, safe ...bool) (dest map[string]interface{}, ok bool) {
	ok = Convert(src, &dest, safe...)
	return
}

func ToBoolMap(src interface{}, safe ...bool) (dest map[string]bool, ok bool) {
	ok = Convert(src, &dest, safe...)
	return
}

func ToRuneMap(src interface{}, safe ...bool) (dest map[string]rune, ok bool) {
	ok = Convert(src, &dest, safe...)
	return
}

func ToByteMap(src interface{}, safe ...bool) (dest map[string]byte, ok bool) {
	ok = Convert(src, &dest, safe...)
	return
}

func ToIntMap(src interface{}, safe ...bool) (dest map[string]int, ok bool) {
	ok = Convert(src, &dest, safe...)
	return
}

func ToUintMap(src interface{}, safe ...bool) (dest map[string]uint, ok bool) {
	ok = Convert(src, &dest, safe...)
	return
}

func ToFloatMap(src interface{}, safe ...bool) (dest map[string]float64, ok bool) {
	ok = Convert(src, &dest, safe...)
	return
}

func ToStringMap(src interface{}, safe ...bool) (dest map[string]string, ok bool) {
	ok = Convert(src, &dest, safe...)
	return
}

func ToSet(src interface{}, safe ...bool) (dest map[interface{}]bool, ok bool) {
	ok = Convert(src, &dest, safe...)
	return
}

func ToBoolSet(src interface{}, safe ...bool) (dest map[bool]bool, ok bool) {
	ok = Convert(src, &dest, safe...)
	return
}

func ToRuneSet(src interface{}, safe ...bool) (dest map[rune]bool, ok bool) {
	ok = Convert(src, &dest, safe...)
	return
}

func ToByteSet(src interface{}, safe ...bool) (dest map[byte]bool, ok bool) {
	ok = Convert(src, &dest, safe...)
	return
}

func ToIntSet(src interface{}, safe ...bool) (dest map[int]bool, ok bool) {
	ok = Convert(src, &dest, safe...)
	return
}

func ToUintSet(src interface{}, safe ...bool) (dest map[uint]bool, ok bool) {
	ok = Convert(src, &dest, safe...)
	return
}

func ToFloatSet(src interface{}, safe ...bool) (dest map[float64]bool, ok bool) {
	ok = Convert(src, &dest, safe...)
	return
}

func ToStringSet(src interface{}, safe ...bool) (dest map[string]bool, ok bool) {
	ok = Convert(src, &dest, safe...)
	return
}

//SetZero réinitialise la variable pointée
//par le paramètre d’entrée à sa valeur initiale
func SetZero(dst interface{}) bool {
	c := NewChecker(dst)
	if c.Kind() != reflect.Ptr {
		return false
	}
	v := c.Value().Elem()
	z := ZeroOf(v.Interface())
	v.Set(reflect.ValueOf(z))
	return true
}

//Clone retourne une copie de l’élément fournit en paramètre.
//Si deep est fourni et vaut true, le clonage s’effectue récursivement.
func Clone(e interface{}, deep ...bool) interface{} {
	c := NewChecker(e)
	if c.IsPointer() {
		if len(deep) == 0 || !deep[0] {
			return e
		}
		n := Clone(ValueOf(e), true)
		return PointerOf(n)
	}
	if c.IsInvalid() {
		return e
	}
	cln := func(v reflect.Value) reflect.Value {
		return reflect.ValueOf(Clone(v.Interface(), deep...))
	}
	t := c.Type()
	vs, vd := c.Value(), reflect.Zero(t)
	switch c.Kind() {
	case reflect.Slice:
		l := vs.Len()
		vd = reflect.MakeSlice(t, l, vs.Cap())
		for i := 0; i < l; i++ {
			s, d := vs.Index(i), vd.Index(i)
			d.Set(cln(s))
		}
	case reflect.Map:
		vd = reflect.MakeMap(t)
		for _, k := range vd.MapKeys() {
			s := vs.MapIndex(k)
			vs.SetMapIndex(k, cln(s))
		}
	case reflect.Struct:
		n := t.NumField()
		for i := 0; i < n; i++ {
			f := t.Field(i)
			if f.PkgPath != "" {
				continue
			}
			s, d := vs.Field(i), vd.Field(i)
			d.Set(cln(s))
		}
	default:
		vd = pointer(t)
		Convert(e, vd.Interface())
		vd = vd.Elem()
	}
	return vd.Interface()
}
