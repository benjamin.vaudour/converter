# converter

converter est une librairie en Go pour convertir des variables entre elles.

## Installation

La librairie peut être installée dans le répertoire $GOPATH via la commande suivante :

```
go get framagit.org/benjamin.vaudour/converter
```

ou bien être utilisée directement dans un projet (nécessite Go ≥ 1.12)  via un import :

```go
import (
	"framagit.org/benjamin.vaudour/converter"
)
```
